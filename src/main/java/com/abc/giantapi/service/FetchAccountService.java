package com.abc.giantapi.service;


import com.abc.giantapi.model.accountservice.AccountResponse;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class FetchAccountService {

    @Value("${accountUrl}")
    private String accountUrl;

    private RestTemplate restTemplate = new RestTemplate();

    public AccountResponse getAccountDetails() {

        try {
            ResponseEntity<AccountResponse> response = restTemplate.getForEntity(accountUrl, AccountResponse.class);
            return response.getBody();
        } catch (Exception e){
            return null;
        }


    }

}
