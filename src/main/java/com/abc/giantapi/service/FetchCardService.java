package com.abc.giantapi.service;


import com.abc.giantapi.model.cardservice.CardResponse;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class FetchCardService {

    @Value("${cardUrl}")
    private String cardUrl;
    private RestTemplate restTemplate = new RestTemplate();

    public CardResponse getCardDetails() {


        try {
            ResponseEntity<CardResponse> response = restTemplate.getForEntity(cardUrl, CardResponse.class);
            return response.getBody();
        } catch (Exception e) {
            return null;
        }

    }

}
