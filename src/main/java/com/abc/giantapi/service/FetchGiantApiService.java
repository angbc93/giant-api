package com.abc.giantapi.service;

import com.abc.giantapi.model.Account;
import com.abc.giantapi.model.Card;
import com.abc.giantapi.model.accountservice.AccountResponse;
import com.abc.giantapi.model.cardservice.CardResponse;
import com.abc.giantapi.model.response.Applicant;
import com.abc.giantapi.model.response.GiantApiResponse;
import com.abc.giantapi.model.response.Status;
import com.abc.giantapi.repository.StatusLookupDAO;
import com.abc.giantapi.repository.StatusLookupRepo;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;


@Service
public class FetchGiantApiService {

    private static final List<String> CARD_PRODUCT_TYPE = Stream
        .of("Visa", "Master", "AMEX")
        .collect(Collectors.toList());
    private static final List<String> ACCOUNT_PRODUCT_TYPE = Stream
        .of("C", "S")
        .collect(Collectors.toList());
    private FetchCardService fetchCardService;
    private FetchAccountService fetchAccountService;
    private StatusLookupRepo statusLookupRepo;

    public FetchGiantApiService(FetchAccountService fetchAccountService, FetchCardService fetchCardService, StatusLookupRepo statusLookupRepo) {
        this.fetchAccountService = fetchAccountService;
        this.fetchCardService = fetchCardService;
        this.statusLookupRepo = statusLookupRepo;
    }

    public GiantApiResponse getGiantApi() {
        //Initialization
        GiantApiResponse giantApiResponse = new GiantApiResponse();
        Applicant applicant = new Applicant();
        Status status = new Status();
        giantApiResponse.setStatus(status);
        giantApiResponse.setApplicant(applicant);
        giantApiResponse.getApplicant().setCards(new ArrayList<>());
        giantApiResponse.getApplicant().setAccounts(new ArrayList<>());

        CardResponse cardResponse = fetchCardService.getCardDetails();
        AccountResponse accountResponse = fetchAccountService.getAccountDetails();

        //If something went wrong
        if (cardResponse == null || accountResponse == null) {
            StatusLookupDAO statusDAO = statusLookupRepo.findStatusByCode("500");
            statusMapper(giantApiResponse, statusDAO);
            return giantApiResponse;
        }

        List<Card> cards = cardResponse.getCards().stream()
            .filter(this::isCard)
            .collect(Collectors.toList());
        giantApiResponse.getApplicant().setCards(cards);

        List<Account> accounts = accountResponse.getAccounts().stream()
            .filter(this::isAccount)
            .collect(Collectors.toList());
        giantApiResponse.getApplicant().setAccounts(accounts);

        //Status Lookup
        StatusLookupDAO statusDAO = statusLookupRepo.findStatusByCode("200");
        statusMapper(giantApiResponse, statusDAO);
        return giantApiResponse;
    }

    private void statusMapper(GiantApiResponse giantApiResponse, StatusLookupDAO status) {
        giantApiResponse.getStatus().setCode(status.getCode());
        giantApiResponse.getStatus().setTitle(status.getTitle());
        giantApiResponse.getStatus().setDescription(status.getDescription());
        giantApiResponse.getStatus().setCreatedBy(status.getCreatedBy());
        giantApiResponse.getStatus().setCreatedTime(status.getCreatedTime());
        giantApiResponse.getStatus().setUpdatedBy(status.getUpdatedBy());
        giantApiResponse.getStatus().setUpdatedTime(status.getUpdatedTime());
    }

    private Boolean isCard(Card card) {
        return CARD_PRODUCT_TYPE.contains(card.getCardProduct().getProductType());
    }

    private Boolean isAccount(Account account) {
        return ACCOUNT_PRODUCT_TYPE.contains(account.getAccountProduct().getProductType());
    }


}
