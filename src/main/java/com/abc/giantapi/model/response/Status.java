package com.abc.giantapi.model.response;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Status implements Serializable {

    private String code;
    private String title;
    private String description;
    private String createdBy;
    private Date createdTime;
    private String updatedBy;
    private Date updatedTime;

}
