package com.abc.giantapi.model.response;


import com.abc.giantapi.model.Account;
import com.abc.giantapi.model.Card;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Applicant implements Serializable {

    private List<Card> cards;
    private List<Account> accounts;

}
