package com.abc.giantapi.model;



import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Account implements Serializable {

    private String accountNo;
    private Boolean isHidden;
    private ProductDetail accountProduct;
    private String createdBy;
    private Date createdTime;
    private String updatedBy;
    private Date updatedTime;

}
