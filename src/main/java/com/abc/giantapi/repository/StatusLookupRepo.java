package com.abc.giantapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface StatusLookupRepo extends JpaRepository<StatusLookupDAO, Long> {

    @Query(value = "SELECT * FROM tbl_status_lookup WHERE code = :code",
        nativeQuery = true)
    StatusLookupDAO findStatusByCode(@Param("code") String code);


}
