package com.abc.giantapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface CardProductRepo extends JpaRepository<CardProductDAO, Integer> {

    @Query(value = "SELECT * FROM tbl_card_product WHERE product_id = :id",
        nativeQuery = true)
    CardProductDAO findProductById(@Param("id") Integer id);


}
