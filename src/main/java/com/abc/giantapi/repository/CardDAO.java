package com.abc.giantapi.repository;


import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name = "tbl_card")
public class CardDAO {

    @Id
    @GeneratedValue
    private Integer id;

    @Column(name = "card_no")
    private String cardNo;

    @Column(name = "is_hidden")
    private Boolean isHidden;

    @Column(name = "product_id")
    private int productId;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "created_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdTime;

    @Column(name = "updated_by")
    private String updatedBy;

    @Column(name = "updated_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedTime;

}
