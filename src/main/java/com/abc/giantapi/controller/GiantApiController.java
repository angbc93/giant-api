package com.abc.giantapi.controller;


import com.abc.giantapi.model.response.GiantApiResponse;
import com.abc.giantapi.service.FetchGiantApiService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GiantApiController {

    private FetchGiantApiService fetchGiantApiService;
    private HttpHeaders httpHeaders;

    public GiantApiController(FetchGiantApiService fetchGiantApiService) {
        this.fetchGiantApiService = fetchGiantApiService;
    }

    @GetMapping(path = "/giant-api")
    public ResponseEntity fetchGiantApi() {
        try {
            GiantApiResponse giantApiResponse = fetchGiantApiService.getGiantApi();
            return new ResponseEntity<>(giantApiResponse, httpHeaders, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e, httpHeaders, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}


