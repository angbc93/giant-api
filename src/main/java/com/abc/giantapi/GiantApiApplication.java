package com.abc.giantapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GiantApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(GiantApiApplication.class, args);
    }

}
