package com.abc.giantapi.service;

import com.abc.giantapi.model.cardservice.CardResponse;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

@RunWith(MockitoJUnitRunner.class)
public class FetchCardServiceTest {

    @Mock
    private RestTemplate restTemplate;
    @InjectMocks
    private FetchCardService fetchCardService = new FetchCardService();

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getCardDetails() {

        ReflectionTestUtils.setField(fetchCardService, "cardUrl", "http://localhost:8081/card-service");
        CardResponse cardResponse = new CardResponse();
        cardResponse.setCards(new ArrayList<>());

        Mockito.when(restTemplate.getForEntity("http://localhost:8081/card-service", CardResponse.class))
            .thenReturn(new ResponseEntity<>(cardResponse, HttpStatus.OK));

        CardResponse actual = fetchCardService.getCardDetails();

        assertEquals(cardResponse, actual);

    }

    @Test
    public void getCardDetailsFails() {

        ReflectionTestUtils.setField(fetchCardService, "cardUrl", "http://localhost:8081/card-service");
        CardResponse cardResponse = new CardResponse();
        cardResponse.setCards(new ArrayList<>());

        Mockito.when(restTemplate.getForEntity("http://localhost:8081/card-service", CardResponse.class))
            .thenThrow(new RuntimeException());

        CardResponse actual = fetchCardService.getCardDetails();

        assertNull(actual);

    }
}