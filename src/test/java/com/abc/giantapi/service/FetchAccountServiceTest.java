package com.abc.giantapi.service;

import com.abc.giantapi.model.accountservice.AccountResponse;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

@RunWith(MockitoJUnitRunner.class)
public class FetchAccountServiceTest {

    @Mock
    private RestTemplate restTemplate;
    @InjectMocks
    private FetchAccountService fetchAccountService = new FetchAccountService();

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getAccountDetails() {
        ReflectionTestUtils.setField(fetchAccountService, "accountUrl", "http://localhost:8080/account-service");
        AccountResponse accountResponse = new AccountResponse();
        accountResponse.setAccounts(new ArrayList<>());

        Mockito.when(restTemplate.getForEntity("http://localhost:8080/account-service", AccountResponse.class))
            .thenReturn(new ResponseEntity<>(accountResponse, HttpStatus.OK));

        AccountResponse actual = fetchAccountService.getAccountDetails();

        assertEquals(accountResponse, actual);

    }

    @Test()
    public void getAccountDetailsFail() {
        ReflectionTestUtils.setField(fetchAccountService, "accountUrl", "http://localhost:8080/account-service");
        AccountResponse accountResponse = new AccountResponse();
        accountResponse.setAccounts(new ArrayList<>());

        Mockito.when(restTemplate.getForEntity("http://localhost:8080/account-service", AccountResponse.class))
            .thenThrow(new RuntimeException());

        AccountResponse actual = fetchAccountService.getAccountDetails();

        assertNull(actual);

    }
}