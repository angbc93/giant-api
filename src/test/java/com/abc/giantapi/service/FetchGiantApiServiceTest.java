package com.abc.giantapi.service;

import com.abc.giantapi.model.Account;
import com.abc.giantapi.model.Card;
import com.abc.giantapi.model.ProductDetail;
import com.abc.giantapi.model.accountservice.AccountResponse;
import com.abc.giantapi.model.cardservice.CardResponse;
import com.abc.giantapi.model.response.Applicant;
import com.abc.giantapi.model.response.GiantApiResponse;
import com.abc.giantapi.model.response.Status;
import com.abc.giantapi.repository.StatusLookupDAO;
import com.abc.giantapi.repository.StatusLookupRepo;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;

@RunWith(MockitoJUnitRunner.class)
public class FetchGiantApiServiceTest {

    @Mock
    private FetchCardService fetchCardService;
    @Mock
    private FetchAccountService fetchAccountService;
    @Mock
    private StatusLookupRepo statusLookupRepo;
    @InjectMocks
    private FetchGiantApiService fetchGiantApiService;

    @Test
    public void getGiantApiSuccess() {

        AccountResponse accountResponse = new AccountResponse(new ArrayList<>());
        CardResponse cardResponse = new CardResponse(new ArrayList<>());
        StatusLookupDAO statusLookupDAO = new StatusLookupDAO();

        Card card = new Card();
        ProductDetail cardProductDetails = new ProductDetail();
        card.setCardProduct(cardProductDetails);
        card.setCardNo("112233");
        card.setIsHidden(Boolean.FALSE);
        card.getCardProduct().setProductId(1);
        card.getCardProduct().setProductCode("001VISA");
        card.getCardProduct().setProductName("Visa Credit Card");
        card.getCardProduct().setProductType("Visa");
        cardResponse.getCards().add(card);

        Account account = new Account();
        ProductDetail accountProductDetails = new ProductDetail();
        account.setAccountProduct(accountProductDetails);
        account.setAccountNo("12345");
        account.setIsHidden(Boolean.FALSE);
        account.getAccountProduct().setProductId(1);
        account.getAccountProduct().setProductCode("001C");
        account.getAccountProduct().setProductName("Basic Current Account");
        account.getAccountProduct().setProductType("C");
        accountResponse.getAccounts().add(account);

        statusLookupDAO.setCode("200");
        statusLookupDAO.setTitle("Happy Days");
        statusLookupDAO.setDescription("All account has been retrieved.");

        //Expected
        GiantApiResponse giantApiResponse = new GiantApiResponse();
        Applicant applicant = new Applicant();
        Status status = new Status();
        applicant.setAccounts(new ArrayList<>());
        applicant.setCards(new ArrayList<>());

        applicant.getAccounts().add(account);
        applicant.getCards().add(card);

        status.setCode("200");
        status.setTitle("Happy Days");
        status.setDescription("All account has been retrieved.");

        giantApiResponse.setStatus(status);
        giantApiResponse.setApplicant(applicant);

        Mockito.when(fetchAccountService.getAccountDetails()).thenReturn(accountResponse);
        Mockito.when(fetchCardService.getCardDetails()).thenReturn(cardResponse);
        Mockito.when(statusLookupRepo.findStatusByCode("200")).thenReturn(statusLookupDAO);

        GiantApiResponse actual = fetchGiantApiService.getGiantApi();

        Assert.assertEquals(giantApiResponse, actual);

    }

    @Test
    public void getGiantApiOneFail() {

        AccountResponse accountResponse = new AccountResponse(new ArrayList<>());
        CardResponse cardResponse = new CardResponse(new ArrayList<>());
        StatusLookupDAO statusLookupDAO = new StatusLookupDAO();

        Card card = new Card();
        ProductDetail cardProductDetails = new ProductDetail();
        card.setCardProduct(cardProductDetails);
        card.setCardNo("112233");
        card.setIsHidden(Boolean.FALSE);
        card.getCardProduct().setProductId(1);
        card.getCardProduct().setProductCode("001VISA");
        card.getCardProduct().setProductName("Visa Credit Card");
        card.getCardProduct().setProductType("Visa");
        cardResponse.getCards().add(card);

        Account account = new Account();
        ProductDetail accountProductDetails = new ProductDetail();
        account.setAccountProduct(accountProductDetails);
        account.setAccountNo("12345");
        account.setIsHidden(Boolean.FALSE);
        account.getAccountProduct().setProductId(1);
        account.getAccountProduct().setProductCode("001C");
        account.getAccountProduct().setProductName("Basic Current Account");
        account.getAccountProduct().setProductType("C");
        accountResponse.getAccounts().add(account);

        statusLookupDAO.setCode("500");
        statusLookupDAO.setTitle("Opps");
        statusLookupDAO.setDescription("Something when Wrong");

        //Expected
        GiantApiResponse giantApiResponse = new GiantApiResponse();
        Applicant applicant = new Applicant();
        Status status = new Status();
        applicant.setAccounts(new ArrayList<>());
        applicant.setCards(new ArrayList<>());

        status.setCode("500");
        status.setTitle("Opps");
        status.setDescription("Something when Wrong");

        giantApiResponse.setStatus(status);
        giantApiResponse.setApplicant(applicant);

        Mockito.when(fetchAccountService.getAccountDetails()).thenReturn(null);
        Mockito.when(fetchCardService.getCardDetails()).thenReturn(cardResponse);
        Mockito.when(statusLookupRepo.findStatusByCode("500")).thenReturn(statusLookupDAO);

        GiantApiResponse actual = fetchGiantApiService.getGiantApi();

        Assert.assertEquals(giantApiResponse, actual);

    }

    @Test
    public void getGiantApiTwoFail() {

        AccountResponse accountResponse = new AccountResponse(new ArrayList<>());
        CardResponse cardResponse = new CardResponse(new ArrayList<>());
        StatusLookupDAO statusLookupDAO = new StatusLookupDAO();

        Card card = new Card();
        ProductDetail cardProductDetails = new ProductDetail();
        card.setCardProduct(cardProductDetails);
        card.setCardNo("112233");
        card.setIsHidden(Boolean.FALSE);
        card.getCardProduct().setProductId(1);
        card.getCardProduct().setProductCode("001VISA");
        card.getCardProduct().setProductName("Visa Credit Card");
        card.getCardProduct().setProductType("Visa");
        cardResponse.getCards().add(card);

        Account account = new Account();
        ProductDetail accountProductDetails = new ProductDetail();
        account.setAccountProduct(accountProductDetails);
        account.setAccountNo("12345");
        account.setIsHidden(Boolean.FALSE);
        account.getAccountProduct().setProductId(1);
        account.getAccountProduct().setProductCode("001C");
        account.getAccountProduct().setProductName("Basic Current Account");
        account.getAccountProduct().setProductType("C");
        accountResponse.getAccounts().add(account);

        statusLookupDAO.setCode("500");
        statusLookupDAO.setTitle("Opps");
        statusLookupDAO.setDescription("Something when Wrong");

        //Expected
        GiantApiResponse giantApiResponse = new GiantApiResponse();
        Applicant applicant = new Applicant();
        Status status = new Status();
        applicant.setAccounts(new ArrayList<>());
        applicant.setCards(new ArrayList<>());

        status.setCode("500");
        status.setTitle("Opps");
        status.setDescription("Something when Wrong");

        giantApiResponse.setStatus(status);
        giantApiResponse.setApplicant(applicant);

        Mockito.when(fetchAccountService.getAccountDetails()).thenReturn(null);
        Mockito.when(fetchCardService.getCardDetails()).thenReturn(null);
        Mockito.when(statusLookupRepo.findStatusByCode("500")).thenReturn(statusLookupDAO);

        GiantApiResponse actual = fetchGiantApiService.getGiantApi();

        Assert.assertEquals(giantApiResponse, actual);

    }
}