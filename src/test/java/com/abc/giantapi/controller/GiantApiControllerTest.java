package com.abc.giantapi.controller;

import com.abc.giantapi.model.Account;
import com.abc.giantapi.model.Card;
import com.abc.giantapi.model.ProductDetail;
import com.abc.giantapi.model.accountservice.AccountResponse;
import com.abc.giantapi.model.cardservice.CardResponse;
import com.abc.giantapi.model.response.Applicant;
import com.abc.giantapi.model.response.GiantApiResponse;
import com.abc.giantapi.model.response.Status;
import com.abc.giantapi.service.FetchGiantApiService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class GiantApiControllerTest {

    @Mock
    private FetchGiantApiService fetchGiantApiService;
    @InjectMocks
    private GiantApiController giantApiController;

    @Test
    public void fetchGiantApi() {

        AccountResponse accountResponse = new AccountResponse(new ArrayList<>());
        CardResponse cardResponse = new CardResponse(new ArrayList<>());

        Card card = new Card();
        ProductDetail cardProductDetails = new ProductDetail();
        card.setCardProduct(cardProductDetails);
        card.setCardNo("112233");
        card.setIsHidden(Boolean.FALSE);
        card.getCardProduct().setProductId(1);
        card.getCardProduct().setProductCode("001VISA");
        card.getCardProduct().setProductName("Visa Credit Card");
        card.getCardProduct().setProductType("Visa");
        cardResponse.getCards().add(card);

        Account account = new Account();
        ProductDetail accountProductDetails = new ProductDetail();
        account.setAccountProduct(accountProductDetails);
        account.setAccountNo("12345");
        account.setIsHidden(Boolean.FALSE);
        account.getAccountProduct().setProductId(1);
        account.getAccountProduct().setProductCode("001C");
        account.getAccountProduct().setProductName("Basic Current Account");
        account.getAccountProduct().setProductType("C");
        accountResponse.getAccounts().add(account);

        GiantApiResponse giantApiResponse = new GiantApiResponse();
        Applicant applicant = new Applicant();
        Status status = new Status();
        applicant.setAccounts(new ArrayList<>());
        applicant.setCards(new ArrayList<>());

        applicant.getAccounts().add(account);
        applicant.getCards().add(card);

        status.setCode("200");
        status.setTitle("Happy Days");
        status.setDescription("All account has been retrieved.");

        giantApiResponse.setStatus(status);
        giantApiResponse.setApplicant(applicant);

        Mockito.when(fetchGiantApiService.getGiantApi()).thenReturn(giantApiResponse);

        ResponseEntity actual = giantApiController.fetchGiantApi();

        assertEquals(HttpStatus.OK,actual.getStatusCode());
        assertEquals(giantApiResponse, actual.getBody());
    }

    @Test
    public void fetchGiantApiFail() {

        Mockito.when(fetchGiantApiService.getGiantApi()).thenThrow(new RuntimeException());

        ResponseEntity actual = giantApiController.fetchGiantApi();

        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR,actual.getStatusCode());
    }
}